<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 20px; 
			background-color: #C4DF9B;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: #005825;
			border-bottom: 1px solid #005825;}
			h2 {font-size: 1.2em;
			color: #4A0048;}
		</style>
	</head>
    <body>


<?php


/** SE CREA EL OBJETO DE CONEXION */
@$link = new mysqli('localhost', 'root', 'kra11oct17', 'marketzone');	

if(isset($_REQUEST['btn-agregar'])){

/** comprobar la conexión */
if ($link->connect_errno) 
{
    die('Falló la conexión: '.$link->connect_error.'<br/>');
    /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
}


    $nombre = $_POST['nombre'];
    $marca  = $_POST['marca'];
    $modelo = $_POST['modelo'];
    $precio = $_POST['precio'];
    $detalles = $_POST['detalles'];
    $unidades = $_POST['unidades'];

    $imagename   = $_FILES['imagen']['name'];
    $temp = $_FILES['imagen']['tmp_name'];
    $carpeta = 'img';
    $imagen =$carpeta.'/'.$imagename;
    move_uploaded_file($temp,$carpeta.'/'. $imagename);


    if(empty($nombre)){
    echo"<p class='error'>* Agrega tu nombre </p>";
    }
    else{
        if(strlen($nombre) > 100 ){
            echo"<p class='error'>* El nombre es muy largo </p>";
        }
    }
    if(empty($marca)){
        echo"<p class='error'>* El campo marca esta vacio </p>";
    }
    if(empty($modelo)){
            echo"<p class='error'>* Agrega el modelo </p>";
    }
    if(empty($precio)){
        echo"<p class='error'>* Agrega el precio </p>";
    }
    else{
        if(!is_numeric($precio)){
            echo"<p class='error'>* Escribe el valor con punto decimal</p>";
        }
    }
    if(empty($detalles)){
        echo"<p class='error'>* Agrega los detalles </p>";
    }
    if(empty($unidades)){
        echo"<p class='error'>* Agrega las unidades </p>";
    }
    else {
        if(!is_numeric($unidades)){
            echo"<p class='error'>* Escribe un número para el campo Unidades</p>";
        }
    }
    if(empty($imagen)){
        echo"<p class='error'>* Agrega imagen </p>";
    }

    $eliminado = 0;

/** Crear una tabla que no devuelve un conjunto de resultados */
$sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}','{$eliminado}')";
if ( $link->query($sql) ) 
{
    echo 'Producto insertado con ID: '.$link->insert_id;
    echo '<br>';
    echo 'Nombre:'.$nombre;
    echo '<br>';
    echo 'Marca:'.$marca;
    echo '<br>';
    echo 'Modelo:'.$modelo;
    echo '<br>';
    echo 'Precio:'.$precio;
    echo '<br>';
    echo 'Detalles:'.$detalles;
    echo '<br>';
    echo 'Unidades:'.$unidades;
    echo '<br>';
    echo 'Eliminado:'.$eliminado;

}
else
{
	echo 'El Producto no pudo ser insertado =(';
}
}
$link->close();
?>

</body>
</html>