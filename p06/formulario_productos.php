<!DOCTYPE html >
<html>

  <head>
    <meta charset="utf-8" >
    <title>Registro de Productos</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>
  </head>

  <body>
    <h1>Registro de Prodctos</h1>

    <form id="formularioTenis" action="http://localhost/tecnologiasweb/practicas/p06/set_producto_v2.php" method="post" enctype="multipart/form-data">

    <h2>Información del Producto</h2>

      <fieldset>
        <legend>Producto</legend>

        <ul>
        <li><label for="form-nombre">Nombre:</label> <input type="text" name="nombre" id="form-nombre"></li>
        <li><label for="form-marca">Marca:</label> <input type="text" name="marca" id="form-marca"></li>
        <li><label for="form-modelo">Modelo:</label> <input type="text" name="modelo" id="form-modelo"></li>
        <li><label for="form-precio">Precio:</label> <input  type="number" step="0.01" min="1" name="precio" id="form-precio"></li>
        <li><label for="form-detalles">Detalles:</label><br><textarea name="detalles" rows="3" cols="35" id="form-detalles" placeholder="No más de 250 caracteres de longitud"></textarea></li>
        <li><label for="form-unidades">Unidades:</label> <input type="number" name="unidades" id="form-unidades"></li>
        <li><label for="form-imagen">Imagen:</label> <input type="file" name="imagen" id="form-imagen"></li>
        </ul>
      </fieldset>

      <p>
        <input type="submit" value="Guardar" name="btn-agregar">
        <input type="reset">
      </p>

    </form>
  </body>
</html>