<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Pagina XHTML de Respuestas</title>
            </head>
        <body>
            <h1 style="text-align:center">Esta pagina muestra las respuestas a la Practica 3 de Tecnologias Web</h1>
            <h2 style="text-align:center">1. Determina cuál de las siguientes variables son válidas y explica por qué:</h2>
            <h3 style="text-align:center">$_myvar, $_7var, myvar, $myvar, $var7, $_element1, $house*5</h3>
            <p style="text-align:center">En PHP la declaración de variable es válida si después del signo de dólar tenemos
            una letra o guion bajo, esto seguido de cualquier número de letras, guiones bajos o números, por lo que en 
            este caso las variables validas son: $_myvar, $_7var, $myvar, $var7 y $_element1 </p> 
            
            <h3>2. Proporcionar los valores de $a, $b, $c como sigue: $a = "ManejadorSQL"; $b = 'MySQL'; $c = &amp;$a;</h3>
            <?php
                $a = "Manejador SQL";
                $b = 'MySQL';
                $c = &$a;
                echo "<h4>Respuesta Inciso a.</h4>";
                echo "<p>$a</p>";
                echo "<p>$b</p>";
                echo "<p>$c</p>"; // Termino del inciso 2a.
            ?>
            <h4>b. Agrega al código actual las siguientes asignaciones: $a = "PHP server"; $b = &amp;$a;</h4>
            <?php
            unset($a);
            unset($b);

            $a = "PHP SERVER";
            $b = &$a; //Termino del inciso 2b
            ?>

            <h4>c. Vuelve a mostrar el contenido de cada uno</h4>
            <?php
                        echo "<h4>Respuesta Inciso c.</h4>";
                        echo "<p>$a</p>";
                        echo "<p>$b</p>";
                        echo "<p>$c</p>"; // Termino del inciso 2c
            ?>

            <h4>d. Describe en y muestra en la página obtenida qué ocurrió en el segundo bloque de asignaciones</h4>
            <p>Lo que paso en la segunda asignacion es que reseteamos las variables $a y $b con la funcion unset() esto para no crear conflictos y asignamos un
                nuevo valor que es el "PHP SERVER" despues en la variable $b apuntamos a la variable $a lo que nos muestra la misma leyenda al 
                imprimir las dos variables que es "PHP SERVER"
            </p>

            <h4>3. Muestra el contenido de cada variable inmediatamente después de cada asignación, verificar la evolución del tipo de estas variables
                (imprime todos los componentes de los arreglo):</h4>
            <?php
            unset($a);
            unset($b);
            unset($c);

            $a = "PHP5";
            echo "<p>$a</p>";
            $z[] = &$a;
            echo "<br>";
            print_r($z);
            $b = "5a version de PHP";
            echo "<p>$b</p>";
            $c = $b * 10;
            echo "<br>";
            print_r($c);
            $a .= $b;
            echo "<br>";
            print_r($a);
            $b *= $c;
            echo "<br>";
            var_dump($b);
            $z[0] = "MySQL";
            echo "<br>";
            print_r($z[0]);
            ?>

            <h4>4. Lee y muestra los valores de las variables del ejercicio anterior, pero ahora con la ayuda de la matriz $GLOBALS o 
                del modificador global de PHP.</h4>
            <?php
                    function globales(){
                    
                        global $a, $b, $c, $z;
                        echo "<p>$a</p>";
                        echo "<p>$b</p>";
                        echo "<p>$c</p>";
                        print_r($z);
                    }
                    globales();  
            ?>

            <h4>5. Dar el valor de las variables $a, $b, $c al final del siguiente script:
            $a = "7 personas";
            $b = (integer) $a;
            $a = "9E3";
            $c = (double) $a;</h4>
            <?php
            unset($a);
            unset($b);
            unset($c);

            $a = "7 personas";
            echo "<p>$a</p>";
            $b = (integer) $a;
            unset($a);
            $a = "9E3";
            $c = (double) $a;

            print_r($b);
            echo "<p>$a</p>";
            print_r($c);
            ?>

            <h4>6. Dar y comprobar el valor booleano de las variables $a, $b, $c, $d, $e y $f y muéstralas usando la función var_dump(datos).</h4>
            <?php
            unset($a);
            unset($b);
            unset($c);

            $a = "0";
            $b = "TRUE";
            $c = FALSE;
            $d = ($a OR $b);
            $e = ($a AND $c);
            $f = ($a XOR $b);

            var_dump($a);
            var_dump($b);
            var_dump($c);
            var_dump($d);
            var_dump($e);
            var_dump($f);

            echo "<br>";

            ?>

            <h4>Después investiga una función de PHP que permita transformar el valor booleano de $c y $e en uno que se pueda mostrar con un echo:</h4>
            <?php

            echo $c ? 'TRUE' : 'FALSE';
            echo "<br>";
            echo $e ? 'TRUE' : 'FALSE';

            ?>

            <h4>7. Usando la variable predefinida $_SERVER, determina lo siguiente:a. La versión de Apache y PHP, b. El nombre del sistema operativo (servidor),
            c. El idioma del navegador (cliente).</h4>

            <?php
            echo $_SERVER['SERVER_SOFTWARE'];
            echo "<br>";
            echo $_SERVER['HTTP_USER_AGENT'];
            echo "<br>";
            echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];        
            ?>

<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
      src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
  </p>
  

        </body>
</html>