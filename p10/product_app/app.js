    var text = document.getElementById("text");
    var text2 = document.getElementById("text2");
    var text3 = document.getElementById("text3");
    var text4 = document.getElementById("text4");
    var text5 = document.getElementById("text5"); 
    var text6 = document.getElementById("text6");
    var text7 = document.getElementById("text7");
    var form = document.getElementById("product-form");

function VerNombre(nombre) {
    if (nombre.value == '') {
        form.classList.add("invalid");
                    form.classList.remove("valid");
                    text.innerHTML = "Introduce un nombre";
                    text.style.color = "#ff0000";
                    text.style.fontFamily ="Arial";
                    text.style.fontSize = "14px";
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text.innerHTML = "______________";
                    text.style.color = "#24B867";

        // let nombre = $('#name').val();

        //     $.ajax({
        //         url: 'backend/product-name.php',
        //         type: 'GET',
        //         data: { nombre },
        //         success: function(response){
        //             console.log(response);
        //             if (response.length > 0) {
        //                 form.classList.add("invalid");
        //                 form.classList.remove("valid");
        //                 text.innerHTML = "Introduce un nombre que no exista";
        //                 text.style.color = "#ff0000";
        //                 text.style.fontFamily ="Arial";
        //                 text.style.fontSize = "14px";

        //             }
        //             else{
        //             form.classList.add("valid");
        //             form.classList.remove("invalid");
        //             text.innerHTML = "______________";
        //             text.style.color = "#24B867";
        //             }
        //         }
        //     })
        
    }
}

function VerPrecio(precio) {
    if (precio.value < 99.99) {
        form.classList.add("invalid");
                    form.classList.remove("valid");
                    text2.innerHTML = "Introduce un precio mayor o igual a 99.99";
                    text2.style.color = "#ff0000";
                    text2.style.fontFamily ="Arial";
                    text2.style.fontSize = "14px";
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text2.innerHTML = "______________";
                    text2.style.color = "#24B867";
    }
}

function VerUnidades(unidades) {
    if (unidades.value <= 0) {
        form.classList.add("invalid");
                    form.classList.remove("valid");
                    text3.innerHTML = "Introduce un numero entero";
                    text3.style.color = "#ff0000";
                    text3.style.fontFamily ="Arial";
                    text3.style.fontSize = "14px";

                    
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text3.innerHTML = "______________";
                    text3.style.color = "#24B867";
    }
}
function VerModelo(modelo) {
    if (modelo.value == '') {
        form.classList.add("invalid");
                    form.classList.remove("valid");
                    text4.innerHTML = "Introduce un modelo";
                    text4.style.color = "#ff0000";
                    text4.style.fontFamily ="Arial";
                    text4.style.fontSize = "14px";
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text4.innerHTML = "______________";
                    text4.style.color = "#24B867";
    }
}
function VerMarca(marca) {
    if (marca.value == '') {
        form.classList.add("invalid");
                    form.classList.remove("valid");
                    text5.innerHTML = "Introduce un modelo";
                    text5.style.color = "#ff0000";
                    text5.style.fontFamily ="Arial";
                    text5.style.fontSize = "14px";
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text5.innerHTML = "______________";
                    text5.style.color = "#24B867";
    }
}
function VerDetalles(detalles) {
    if (detalles.value == '') {
        detalles.value = 'N/A';
        // form.classList.add("invalid");
        //             form.classList.remove("valid");
        //             text4.innerHTML = "Introduce un modelo";
        //             text4.style.color = "#ff0000";
        //             text4.style.fontFamily ="Arial";
        //             text4.style.fontSize = "14px";
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text6.innerHTML = "______________";
                    text6.style.color = "#24B867";
    }
}

function VerImagen(imagen) {
    if (imagen.value == '') {
        imagen.value = 'img/imagen.png';
        // form.classList.add("invalid");
        //             form.classList.remove("valid");
        //             text4.innerHTML = "Introduce un modelo";
        //             text4.style.color = "#ff0000";
        //             text4.style.fontFamily ="Arial";
        //             text4.style.fontSize = "14px";
    }
    else{
                    form.classList.add("valid");
                    form.classList.remove("invalid");
                    text7.innerHTML = "______________";
                    text7.style.color = "#24B867";
    }
}



$(document).ready(function(){
    let edit = false;

    $('#product-result').hide();
    listarProductos();

/////////////////////////////////////////////////// MOSTRAR PRODUCTOS ///////////////////////////////////////////////////////

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                const productos = JSON.parse(response);
            
                // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                if(Object.keys(productos).length > 0) {
                    // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                    let template = '';

                    productos.forEach(producto => {
                    
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                <td><ul>
                                <li>precio: ${producto.precio}</li>
                                <li>unidades: ${producto.unidades}</li>
                                <li>modelo: ${producto.modelo}</li>
                                <li>marca: ${producto.marca}</li>
                                <li>detalles: ${producto.detalles}</li>
                                </ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger" onclick="eliminarProducto()">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    //console.log(producto);
                    $('#products').html(template);
                }
            }
        });
    }



/////////////////////////////////////////////////// BUSCAR PRODUCTO ///////////////////////////////////////////////////////
    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#search').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                        const productos = JSON.parse(response);
                        
                        // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                        if(Object.keys(productos).length > 0) {
                            // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                            let template = '';
                            let template_bar = '';

                            productos.forEach(producto => {
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>
                                        <li>precio: ${producto.precio}</li>
                                        <li>unidades: ${producto.unidades}</li>
                                        <li>modelo: ${producto.modelo}</li>
                                        <li>marca: ${producto.marca}</li>
                                        <li>detalles: ${producto.detalles}</li>
                                        </ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // SE HACE VISIBLE LA BARRA DE ESTADO
                            $('#product-result').show();
                            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                            $('#container').html(template_bar);
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide();
        }
    });

    $('#name').keyup(function() {
        if($('#name').val()) {
            let namsear = $('#name').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#name').val(),
                data: {namsear},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                        const productos = JSON.parse(response);
                        
                        // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                        if(Object.keys(productos).length > 0) {
                            // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                            //let template = '';
                            let template_bar = '';
    
                            productos.forEach(producto => {
                            
                                if(producto.nombre == namsear){
                                template_bar += `
                                    <li>Ya existe un producto con ese nombre</il>
                                `;
                                }
                            });
                            // SE HACE VISIBLE LA BARRA DE ESTADO
                            $('#product-result').show(template_bar);
                            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                            $('#container').html(template_bar);
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            //$('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide();
        }
    });

/////////////////////////////////////////////////// ENVIAR PRODUCTO ///////////////////////////////////////////////////////

    $('#product-form').submit(e => {
        e.preventDefault();

        // SE CONVIERTE EL JSON DE STRING A OBJETO
        const postData = {
        // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
        nombre: $('#name').val(),
        precio: $('#precio').val(),
        unidades: $('#unidades').val(),
        marca: $('#marca').val(),
        modelo: $('#modelo').val(),
        detalles: $('#detalles').val(),
        imagen: $('#imagen').val(),

        id : $('#productId').val(),
    };

    
    
    // var text = document.getElementById("text");
    // var form =document.getElementById("product-form");

    //     if (postData["nombre"] == ''){
           
    //             form.classList.add("invalid");
    //             form.classList.remove("valid");
    //             text.innerHTML = "Introduce un nombre";
    //               text.style.color = "#ff0000";
    //       }
    //       else {
    //           form.classList.add("valid");
    //           form.classList.remove("invalid");
    //          text.innerHTML = "Correcto";
    //           text.style.color = "#00ff00";
            
    //       }

        const url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';
        
        $.post(url, postData, (response) => {
            console.log(response);
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let respuesta = JSON.parse(response);
            // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">status: ${respuesta.status}</li>
                        <li style="list-style: none;">message: ${respuesta.message}</li>
                    `;
            // SE REINICIA EL FORMULARIO
            $('#name').val('');
            $('#precio').val('');
            $('#unidades').val('');
            $('#modelo').val('');
            $('#marca').val('');
            $('#detalles').val('');
            $('#imagen').val('');
            // SE HACE VISIBLE LA BARRA DE ESTADO
            $('#product-result').show();
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            $('#container').html(template_bar);
            // SE LISTAN TODOS LOS PRODUCTOS
            listarProductos();
            // SE REGRESA LA BANDERA DE EDICIÓN A false
            edit = false;
        });
    });
/////////////////////////////////////////////////// ELIMINAR PRODUCTO ///////////////////////////////////////////////////////
    $(document).on('click', '.product-delete', (e) => {
        if(confirm('¿Realmente deseas eliminar el producto?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('./backend/product-delete.php', {id}, (response) => {
                $('#product-result').hide();
                listarProductos();
            });
        }
    });
/////////////////////////////////////////////////// EDITAR PRODUCTO ///////////////////////////////////////////////////////
    $(document).on('click', '.product-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId');
        $.post('./backend/product-single.php', {id}, (response) => {
            // SE CONVIERTE A OBJETO EL JSON OBTENIDO
            let product = JSON.parse(response);
            // SE INSERTAN LOS DATOS ESPECIALES EN LOS CAMPOS CORRESPONDIENTES
            $('#name').val(product.nombre);
            $('#precio').val(product.precio);
            $('#unidades').val(product.unidades);
            $('#modelo').val(product.modelo);
            $('#marca').val(product.marca);
            $('#detalles').val(product.detalles);  
            $('#imagen').val(product.imagen);  
            // EL ID SE INSERTA EN UN CAMPO OCULTO PARA USARLO DESPUÉS PARA LA ACTUALIZACIÓN
            $('#productId').val(product.id);

            
            // SE PONE LA BANDERA DE EDICIÓN EN true
            edit = true;
        });
        e.preventDefault();
    });    
});