<?php
//namespace BACKEND\API;
namespace API\DataBase;

abstract class DataBase {
    protected $conexion;
    protected $response;

    public function __construct($database) {

        $this->conexion = @mysqli_connect(
            'localhost',
            'root',
            'kra11oct17',
            "{$database}"
        );
    
        /**
         * NOTA: si la conexión falló $conexion contendrá false
         **/
        if(!$this->conexion) {
            die('¡Base de datos NO conextada!');
        }
        /*else {
            echo 'Base de datos encontrada';
        }*/
        $this->response = array();
    }
    // public function __FUNCTION__($database='marketzone') {
    //     $this->response = array();
    //     parent::__construct($database);
    // }

    public function getResponse() {
        // SE HACE LA CONVERSIÓN DE ARRAY A JSON

        return json_encode($this->response, JSON_PRETTY_PRINT);
    }
}
?>