<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

    <?php

        if(isset($_GET['tope']))
        {
            $tope = $_GET['tope'];
        }
        else
        {
            die('Introduce el tope');
        }

        if (!empty($tope)) {
         
            @$link = new mysqli('localhost', 'root', 'kra11oct17', 'marketzone');

      
            if ($link->connect_errno) 
            {
                die('Falló la conexión: '.$link->connect_error.'<br/>');
     
            }
        }

      
        if ( $result = $link->query("SELECT * FROM productos WHERE unidades <= $tope") ) 
        {
            $conSQL = "SELECT * FROM productos WHERE unidades <= $tope";
            $row = $result->fetch_all(MYSQLI_ASSOC);
            $query = mysqli_query($link,$conSQL);
            
            $result->free();
        }

        $link->close();
    ?>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php
            echo "<title>Productos con Unidades <= $tope</title>";
        ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
	<body>
        <script>
            function data() {
              
                var rowId = event.target.parentNode.parentNode.id;
                var data = document.getElementById(rowId).querySelectorAll(".row-data");

                var id1 = data[0].innerHTML;
                var nombre1 = data[1].innerHTML;
                var modelo1 = data[3].innerHTML;
                var unidades1 = data[6].innerHTML;

                var url1 = "http://localhost/tecnologiasweb/p7/practica/formulario_productos_v2.php";
                window.open(url1+"?id="+id1+"&nombre="+nombre1+"&modelo="+modelo1+"&unidades="+unidades1);

            }
        </script>
		<br/>

		<table class="table">
			<thead class="thead-dark">
				<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
		    	<th scope="col">Marca</th>
				<th scope="col">Modelo</th>
				<th scope="col">Precio</th>
                <th scope="col">Detalles</th>
				<th scope="col">Unidades</th>
				<th scope="col">Imagen</th>
                <th scope="col">Modificar</th>
				</tr>
			</thead>
			<tbody>
                <?php foreach ($query as $objeto): ?> 
                <tr id="<?=$objeto['id']?>">
						<th class="row-data"><?= $objeto['id'] ?></th>
						<td class="row-data"><?= $objeto['nombre'] ?></td>
						<td class="row-data"><?= $objeto['marca'] ?></td>
						<td class="row-data"><?= $objeto['modelo'] ?></td>
						<td class="row-data"><?= $objeto['precio'] ?></td>
						<td class="row-data"><?= utf8_encode($objeto['detalles']) ?></td>
                        <td class="row-data"><?= $objeto['unidades'] ?></td>
						<td class="row-data"><img src=<?= $objeto['imagen'] ?> widht="150px" height="150px"></td>
                        <td><input type="button" value="Editar" onClick="data()"/></td>
				</tr>
                <?php endforeach; ?>
			</tbody>
		</table>
    </body>
</html>