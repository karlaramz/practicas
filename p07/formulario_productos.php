<!DOCTYPE html >
<html>

    <head>
    <meta charset="utf-8" >
    <title>Modificar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style type="text/css">
    ol, ul { 
    list-style-type: none;
    }
    
    body{
        margin: 40px;
        padding-left: 40px;
        padding-right: 40px;
        }
    </style>
    </head>

    <body>
    <script type="text/javascript">
        function vernombre(nombre) {
        if (nombre.value == ''){
            alert('Introduce un nombre');
        }
        else if (nombre.lenght >= 100) {
            alert('Introduce un nombre menor o igual a 100 caracteres');
        }
        }

        function vermodelo(modelo) {
        if (modelo.value == '') {
            alert('Introduce un modelo');
        }
        else if (modelo.lenght > 25) {
            alert('El modelo debe tener menos de 25 caracteres');
        }
        }

        function verprecio(precio) {
        if (precio.value == '') {
            alert('Introduce un precio');
        }
        else if (precio.value < 99.99) {
            alert('Introduce un precio mayor a $99.99');
        }
        }

        function verdetalles(detalles) {
        if (detalles.lenght > 250) {
            alert('Ingresa detalles con menos de 250 caracteres');
        }
        }

        function verrunidades(unidades) {
        parseInt(unidades);
        if (unidades.value < 0) {
            alert('Introduce un numero entero');
        }
        }

        function verimg(img) {
        if (img.value == '') {
        img.value = 'img/imagen.png';
        }
        }

    </script>
 
    <h1 align="center"> Registro Nuevo Smartphone</h1>
    <h2>Informacion de Smartphone</h2>
    <br>
    <form  class="row g-3" id="formularioTenis" action="http://localhost/tecnologiasweb/practicas/p07/get_productos_xhtml.php" method="post">

 
            <div class="col-md-6">
            <label for="form-nombre" class="form-label">Nombre</label>
            <input class="form-control" type="text" name="nombre" id="form-nombre" onblur="vernombre(this)" >
            </div>

            <div class="col-md-6">
            <label for="form-marca" class="form-label">Marca</label>
                <select id="form-marca" class="form-select" name="marca">
                <option>Apple</option>
                <option>Samsung</option>
                <option>Motorola</option>
                <option>Huawei</option>
                <option>Xiaomi</option>
                <option>Oppo</option>
                </select>
            </div>

            <div class="col-md-6">
            <label for="form-modelo" class="form-label">Modelo</label>
            <input class="form-control" type="text" name="modelo" id="form-modelo" maxlength="25" onblur="vermodelo(this)" >
            </div>
    
            <div class="col-md-6">
            <label for="form-precio" class="form-label">Precio</label>
            <input class="form-control" type="number"step="0.01" name="precio" id="form-precio" required onblur="verprecio(this)">
            </div>

            <div class="col-md-6"> 
            <label for="form-detalles" class="form-label">Detalles*</label>
            <textarea class="form-control" name="detalles" id="form-detalles" rows="1" onblur="verdetalles(this)" >
            </textarea>
            </div>

            <div class="col-md-6">
            <label for="form-unidades" class="form-label">Unidades</label>
            <input class="form-control" type="text" name="unidades" id="form-unidades" required onblur="verunidades(this)">
            </div>

            <div class="col-md-6"> 
            <label for="form-imagen" class="form-label">Imagen*</label>
            <input class="form-control" type="text" name="imagen" id="form-imagen" onblur="verimg(this)">
            </div>

 
            </select>
            <br>
            <p>* Estos son campos opcionales</p>

    <p>
        <button type="submit" class="btn btn-primary">Agregar</button>
        <button type="reset" class="btn btn-primary">Restablecer</button>
    </p>

    </form>
    </body>
</html>