// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;

    // SE LISTAN TODOS LOS PRODUCTOS
    //listarProductos();
}
//BUSCAR PRODUCTO
$(document).ready(function(){
    let edit = false;
    console.log('jQuery is Working');
    $('#product-result').hide();
    fetchProducts();
    $('#search').keyup(function(e){
        if($('#search').val()){
        let search = $('#search').val();
        $.ajax({
            url:'backend/product-search.php',
            type: 'GET',
            data: { search },
            success: function(response){
                // console.log(respose);
                let products = JSON.parse(response);
                let template = '';
                let template_1 = '';

                products.forEach(product => {
                    template += `<li>
                        ${product.nombre}
                    </li>`
                });
                //console.log(products);
                $('#container').html(template);
                $('#product-result').show();
                products.forEach(product => {
                    template_1 +=`<tr productID="${product.id}">
                    <td>${product.id}</td>
                    <td>${product.nombre}</td>
                    <td><ul>
                    <li>precio: ${product.precio}</li>
                    <li>unidades: ${product.unidades}</li>
                    <li>modelo: ${product.modelo}</li>
                    <li>marca: ${product.marca}</li>
                    <li>detalles: ${product.detalles}</li>
                    </ul></td>
                    <td>
                    <button class="product-delete btn btn-danger">
                        Eliminar
                    </button>    
                </td>
                </tr>`
                });
                $('#products').html(template_1);
            }
            })
        }
    });
//ENVIAR PRODUCTO
    $('#product-form').submit(function(e){

        let data = JSON.parse($('#description').val());
        const postData = {
            nombre: $('#name').val(),
            precio: data["precio"],
            unidades: data["unidades"],
            modelo: data["modelo"],
            marca: data["marca"],
            detalles: data["detalles"],
            imagen: data["imagen"],
            id: $('#product_id').val()
        };
        if(data["precio"] < 99.99 ){
            alert('La modificacion fallo --Introduce un precio mayor a 99.99');
        }
        
        if(data["unidades"] <= 0 ){
            alert('La modificacion fallo --Introduce un numero entero');
        }
        if(data["modelo"] == ''){
            alert('La modificacion fallo --Introduce un modelo');
        }
        if(data["marca"] == ''){
            alert('La modificacion fallo --Introduce una marca');
        }
        if(data["detalles"].length > 250){
            alert('La modificacion fallo --Ingresa detalles con menos de 250 caracteres');
        }
        if(data["imagen"] == ''){
            data["imagen"] = 'img/imagen.png';
        }

        let url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
        console.log(postData, url);

        productoJsonString = JSON.stringify(postData,null,2);
        console.log(productoJsonString);

        $.post(url, productoJsonString, function(response) {
            console.log(response);
            let respuesta = JSON.parse(response);
            fetchProducts();
            $('#product-form').trigger('reset'); 
            let answer = respuesta.message;
            alert(answer);
        });

        //console.log(postData);
        e.preventDefault;
    });
//MOSTRAR PRODUCTOS
    function fetchProducts(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function (response){
                let products = JSON.parse(response);
                let template = '';
                //let descripcion = '';
                    // descripcion += '<li>precio: '+product.precio+'</li>';
                    // descripcion += '<li>unidades: '+product.unidades+'</li>';
                    // descripcion += '<li>modelo: '+product.modelo+'</li>';
                    // descripcion += '<li>marca: '+product.marca+'</li>';
                    // descripcion += '<li>detalles: '+product.detalles+'</li>';
                
                products.forEach(product => {
                    template +=`<tr productID="${product.id}">
                    <td>${product.id}</td>
                    <td>
                        <a href="#" class="product-item">${product.nombre}</a>
                    </td>
                    <td><ul>
                    <li>precio: ${product.precio}</li>
                    <li>unidades: ${product.unidades}</li>
                    <li>modelo: ${product.modelo}</li>
                    <li>marca: ${product.marca}</li>
                    <li>detalles: ${product.detalles}</li>
                    </ul></td>
                    <td>
                    <button class="product-delete btn btn-danger">
                        Eliminar
                    </button>    
                </td>
                </tr>`
                });
                $('#products').html(template);
            }
        });
    }
//ELIMINAR PRODUCTO
    $(document).on('click','.product-delete', function(){
    let element = $(this)[0].parentElement.parentElement;
    let id = $(element).attr('productID');
    if(confirm('¿Deseas eliminar el producto?')){
    $.post('backend/product-delete.php', {id}, function(response){
        let respuesta = JSON.parse(response);
        fetchProducts();
        console.log(response);
        let msj = respuesta.message;
        alert(msj);
        });
    }
    });

    $(document).on('click','product-item', function(){
        console.log("editing");
    });

    $(document).on('click', '.product-item', function(){
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        $.post('backend/product-single.php', {id}, function(response){
            const producto = JSON.parse(response);
            $('#name').val(producto.nombre);
            $('#product_id').val(producto.id);

            var atributosobj = {
                "precio": producto.precio,
                "unidades": producto.unidades,
                "modelo": producto.modelo,
                "marca": producto.marca,
                "detalles": producto.detalles,
                "imagen": producto.imagen
            };

            var objstring = JSON.stringify(atributosobj,null,2);
            $('#description').val(objstring);
            edit = true;
        })
    });


});