<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Pagina XHTML con Funciones</title>
            </head>
        <body>

        <h4>1. Escribir programa para comprobar si un número es un múltiplo de 5 y 7.</h4>
            

        <form action="p03_funciones.php" method="get">
        Ingresa un numero: <input type="text" name="numero">
        <input type="submit">
        </form>

        <h4>2. Crea un programa para la generación repetitiva de 3 números aleatorios hasta obtener una
        secuencia compuesta por: impar, par, impar</h4>

        <h4>3. Utiliza un ciclo while para encontrar el primer número entero obtenido aleatoriamente,
        pero que además sea múltiplo de un número dado. Después crea una variante de este script utilizando el ciclo do-while.</h4>
        <form action="p03_funciones.php" method="get">

        Ingresa un numero como multiplo(Ciclo While): <input type="text" name="num">
        Ingresa un numero como multiplo(Ciclo Do-While): <input type="text" name="nume">
        <input type="submit">
        </form>



        </body>
</html>